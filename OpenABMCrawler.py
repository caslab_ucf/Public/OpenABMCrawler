import scrapy
from items import DownloadedFiles
import time
from scrapy.http import Request

class OpenABMSpider(scrapy.Spider):
		name = 'OpenABMSpider'
		
		start_urls = ['https://www.openabm.org/models']
		custom_settings = {
			'ITEM_PIPELINES' : {'scrapy.pipelines.files.FilesPipeline':1},
			'FILES_STORE' : 'C:\Users\ch328575\Documents\Projects\ODDs',
			'DOWNLOAD_FAIL_ON_DATALOSS' : False,
		} 
		def start_requests(self):
			for i in range(1, 29):
				self.start_urls.append('https://www.openabm.org/models?page=' + str(i) );
			for url in self.start_urls:
				yield scrapy.Request(url=url, callback=self.parse)
		def parse(self, response):			
			#time.sleep(0.2)	
			#Go into models
			model_links = response.xpath('//a[contains(@href, "/model/" )]')
			for index, model_link in enumerate(model_links):
				full_model_link = response.urljoin(model_link.xpath('@href').extract()[0])
				print(full_model_link)
				yield scrapy.Request(full_model_link, callback=self.parseModelPage)
				
			
			
			#To go to next page
			#links = response.xpath('//a[contains(@title, "next" )]')
			#next_page = links[0].xpath('@href').extract()
			#next_page = response.urljoin(next_page[0])
			#print(next_page)		
			#if next_page is not None:
				#return scrapy.Request(next_page, callback=self.parse)

		def parseModelPage(self, response):
			model_descriptions = response.xpath('//a[contains(@type, "application")]')
			print("FOUND DOCUMENTATION: ")
			for index, model_description in enumerate(model_descriptions):
				print(model_description.xpath('@href').extract()[0])
				yield Request(model_description.xpath('@href').extract()[0], callback=self.save_pdf)
				
		def save_pdf(self, response):
			
			file_name = response.url.split('/')[-1]
			file_name = file_name[:-11]
			print(file_name)
			path = "ODDs/" + file_name + " "
			self.logger.info('Saving PDF %s', path)
			with open(path, 'wb') as f:
				f.write(response.body)
